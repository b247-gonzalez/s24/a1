const getCube = (inp_num) => console.log(`The cube of ${inp_num} is ${inp_num ** 3}`);
getCube(2);

let address = ["Vista Verde Exec. Village", "Caybiga", "Caloocan", 1420];
let [address1, town, state, zip_code] = address;

console.log(`I live at ${address1}, ${town}, ${state} City ${zip_code}`);

const animal = {
    animalName: 'Lolong',
    animalSpecies: "crocodile",
    animalHabitat: "saltwater",
    animalWeight: 1075,
    animalMeasurement: "20 ft 3 in"
}

let { animalName, animalSpecies, animalHabitat, animalWeight, animalMeasurement } = animal;

console.log(`${animalName} was a ${animalHabitat} ${animalSpecies}. He weighed at ${animalWeight} kgs with a measurement of ${animalMeasurement}.`);

let numbers = [1,2,3,4,5];

numbers.forEach((number) => {
    console.log(number);
})

let reduceNumber = [1,2,3,4,5].reduce((sum,current) => {
    return sum + current;
});
console.log(reduceNumber);

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);